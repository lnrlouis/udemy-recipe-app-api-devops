variable "prefix" {
  type    = string
  default = "llenoir-raad"
}

variable project {
  type    = string
  default = "llenoir-udemy-recipe-app"
}

variable contact {
  type    = string
  default = "llenoir@ippon.fr"
}
